<!DOCTYPE html>
<html lang="en">
<head>
    <title>Reference</title>

    <link rel="stylesheet" type="text/css" href="css/page-reference.css">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i"
          rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Bitter:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap"
          rel="stylesheet">
</head>
<body class="page-background">
<div class="output-container" id="output-container">

    <page class="page page-preview page-reference" size="Letter" style="background-color:white">
        <div class="page-reference-inner">


            <h1>Army Lists</h1>
            {{#each armyLists}}

                <h2 class="heading">{{name}}</h2>

                <table class="army-list">
                    <thead>
                    <tr>
                        <th>Unit</th>
                        <th>Count</th>
                    </tr>
                    </thead>
                    <tbody>

                    {{#each list}}
                        <tr>
                            <td>{{unit.name}}</td>
                            <td>{{count}}</td>
                        </tr>
                    {{/each}}
                    <tr>
                        <td>model count total</td>
                        <td>{{modelCount}}</td>
                    </tr>

                    </tbody>
                </table>
            {{/each}}

            <h2 class="heading">Definitions</h2>

            {{#each definitions}}
                <p>
                    <strong>{{name}}: </strong>{{{desc}}}
                </p>
            {{/each}}
        </div>
    </page>

</div>
</body>
</html>