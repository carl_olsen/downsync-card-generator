export const SIZE_SMALL = 'Small';
export const SIZE_MEDIUM = 'Medium';
export const SIZE_LARGE = 'Large';

export const TYPE_VEHICLE = 'Vehicle';
export const TYPE_INFANTRY = 'Infantry';
