import { generateArmyListPages, generateCardPages, generateReferencePages } from './generate-pages.js'
import { getTimestamp } from './util.js'
import { COALITION_UNITS } from '../data/cards/coalition-units.js'
import { REPUBLIC_UNITS } from '../data/cards/republic-units.js'
import * as definitions from '../data/definitions.js'
import { armyLists } from '../data/army-lists.js'

let units = [].concat(
    COALITION_UNITS,
    REPUBLIC_UNITS,
    //NANO_SWARM_UNITS,
)

export default function () {

    let cardData = units.map((unit) => {
        unit.timestamp = getTimestamp()

        return unit
    })

    let defs = Object.keys(definitions).map((key) => {
        return definitions[key]
    })

    generateCardPages({
        cardData,
    })

    generateArmyListPages({
        armyListData: {
            armyLists,
        },
    })

    generateReferencePages({
        referenceData: {
            definitions: defs,
        },
    })
}
